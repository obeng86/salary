module.exports = {
    reactStrictMode: true,
    env: {
      IPSTACK_API: process.env.IPSTACK_API,
      IPINFO_TOKEN: process.env.IPINFO_TOKEN,
    }
  }