import mainStyles from '../styles/main.module.css'

export default function AUForm (props) {
    const GrossSalary = props.salary * 1

    // Salary variables
    const yearlyGross = GrossSalary
    const monthlyGross = (GrossSalary)/12
    const weeklyGross = (GrossSalary)/52
    const dailyGross = (GrossSalary)/365

    // Tax paid variables

    const yearlyTax = (x) => {
        let tax
        if (x > 180000) {
            tax = (x - 180000) * .45 + 51667
            return tax
        } else if (x > 120000) {
            tax = (x - 120000) * .37 + 29467
            return tax
        } else if (x > 45000) {
            tax = (x - 45000) * .325 + 5092
            return tax
        } else if (x > 18200) {
            tax = (x - 18200) * .19
            return tax
        } else {
            tax = 0
            return tax
        }
    }
    const monthlyTax = yearlyTax(GrossSalary)/12
    const weeklyTax = yearlyTax(GrossSalary)/52
    const dailyTax = yearlyTax(GrossSalary)/365

    //National Insurance variables
    const yearlyMedicare = (x) => {
        let medicare
        if (x > 18200) {
            medicare = (x - 18200) * .02 
            return medicare
        } else {
            medicare = 0
            return medicare
        }
    }
    const monthlyMedicare = yearlyMedicare(GrossSalary)/12
    const weeklyMedicare = yearlyMedicare(GrossSalary)/52
    const dailyMedicare = yearlyMedicare(GrossSalary)/365

    // Take Home variables

    const yearlyTakeHome = GrossSalary - yearlyTax(GrossSalary) - yearlyMedicare(GrossSalary)
    const monthlyTakeHome = yearlyTakeHome/12
    const weeklyTakeHome = yearlyTakeHome/52
    const dailyTakeHome = yearlyTakeHome/365

    return (
        <>
        <div className={mainStyles.formContentWrapper}>
            <div className={mainStyles.formContent}>
                <form className={mainStyles.form} onSubmit={props.handleSubmit}>
                    <div className={mainStyles.formFirstRow}>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Gross Salary</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <input onChange={props.handleSalaryInput} className={mainStyles.inputText} name="salary" type="text" placeholder="0" value={props.salary}/>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warning}>Please enter an email</p>
                            )} */}
                        </div>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Tax Year</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <select className={mainStyles.inputSelect} onChange={props.handleTaxYearInput}>
                                        <option value="2021-22">2021-22</option>
                                    </select>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warning}>Please enter an email</p>
                            )} */}
                        </div>
                    </div>
                </form>
                <div className={mainStyles.resultsWrapper}>
                    <table className={mainStyles.table}>
                        <thead>
                            <tr>
                                <th className={mainStyles.th}></th>
                                <th className={mainStyles.th}>Yearly</th>
                                <th className={mainStyles.th}>Monthly</th>
                                <th className={mainStyles.th}>Weekly</th>
                                <th className={mainStyles.th}>Daily</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th className={mainStyles.th}>Gross Salary</th>
                                <td className={mainStyles.td}>{yearlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyGross.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Tax</th>
                                <td className={mainStyles.td}>{yearlyTax(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTax.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Medicare Levy</th>
                                <td className={mainStyles.td}>{yearlyMedicare(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyMedicare.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyMedicare.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyMedicare.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Net Pay</th>
                                <td className={mainStyles.td}>{yearlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTakeHome.toFixed(2)}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </>
    )
}