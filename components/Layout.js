import Head from 'next/head'
import Link from 'next/link'
import mainStyles from '../styles/main.module.css'
import footerStyles from '../styles/footer.module.css'
import Countries from './Countries'
import GBForm from './GBForm'
import USForm from './USForm'
import AUForm from './AUForm'
import GHForm from './GHForm'
import CAForm from './CAForm'
import NGForm from './NGForm'
import Header from './Header'
import { useState, useEffect } from 'react'


export const siteTitle = "Calculate your take home pay from your gross salary"

export default function Layout({ children, home }) {

    const [salary, setSalary] = useState(null)
    const [taxYear, setTaxYear] = useState("2021-22")
    const [backgroundFlag, setBackgroundFlag] = useState("")
    const [currentCountryThumbnail, setCurrentCountryThumbnail] = useState("")
    const [currentCountry, setCurrentCountry] = useState("")
    const [currentCountryName, setCurrentCountryName] = useState("")
    const [currentRegion, setRegion] = useState("")

    const ipApi = process.env.IPINFO_TOKEN
    
    useEffect(() => {
        fetch(`https://ipinfo.io/json?token=${ipApi}`)
        .then(res => {
            return res.json();
        })
        .then(data => {
            const country = data.country
            const region = data.region

            for (let i=0; i<Countries.length; i++) {
                if (Countries[i].id == country) {
                    setBackgroundFlag(Countries[i].flag)
                    setCurrentCountryThumbnail(Countries[i].flagThumb)
                    setCurrentCountryName(Countries[i].name)
                    setCurrentCountry(country)
                    setRegion(region)
                }
            }
            console.log(country)
        })
    }, [])
    

    const handleSetSalary = (e) => {
        setSalary(e.target.value)
    }
    const handleTaxYear = (e) => {
        setTaxYear(e.target.value)
    }
    const changeCountry = (e) => {
        const country = e.target.textContent
        const mobileMenu = document.querySelector('[class^="header_mainMenu_"]')
        const countryMenu = document.querySelector('[class^="header_flags_"]')

        for (let i=0; i<Countries.length; i++) {
            if (Countries[i].name == country) {
                setBackgroundFlag(Countries[i].flag)
                setCurrentCountryThumbnail(Countries[i].flagThumb)
                setCurrentCountryName(Countries[i].name)
                setCurrentCountry(Countries[i].id)
            }
        }
        
        mobileMenu.removeAttribute('id')
        countryMenu.removeAttribute('id')
    }
    
    return (
        
        <>
            <Head>
                <link rel="icon" href="/favicon.ico" />
                <meta
                    name="description"
                    content="Learn how to build a personal website using Next.js"
                />
                <meta 
                    property="og:image"
                    content={`https://og-image.vercel.app/${encodeURI(
                        siteTitle
                    )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
                />
                <meta name="og:title" content={siteTitle} />
                <meta name="twitter:card" content="summary_large_image" />
                <link rel="preconnect" href="https://fonts.googleapis.com" />
                <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
                <link href="hhttps://fonts.googleapis.com/css2?family=Inter:wght@100;300;400;500;600;700;800&family=Open+Sans+Condensed:wght@700&family=Open+Sans:wght@600;700;800&display=swap" rel="stylesheet" /> 
            </Head>
            <Header 
                countryID={currentCountry}
                countryName={currentCountryName}
                countryThumbnail={currentCountryThumbnail}
                handleCountryChange={changeCountry}
            />
            
            <main className={mainStyles.mainContainer} style={{backgroundImage: 'url(' + backgroundFlag + ')'}}>
                {home ? (
                    <>
                            {(function() {
                                switch(currentCountry) {
                                    case 'GH':
                                        return <GHForm
                                            handleSalaryInput={handleSetSalary}
                                            salary={salary}
                                            handleTaxYearInput={handleTaxYear}
                                             />;
                                    case 'GB':
                                        return <GBForm 
                                            handleSalaryInput={handleSetSalary}
                                            salary={salary}
                                            handleTaxYearInput={handleTaxYear}
                                            />;
                                    case 'US':
                                        return <USForm
                                            region={currentRegion}
                                            handleSalaryInput={handleSetSalary}
                                            salary={salary}
                                            handleTaxYearInput={handleTaxYear}
                                            />;
                                    case 'AU':
                                        return <AUForm
                                            handleSalaryInput={handleSetSalary}
                                            salary={salary}
                                            handleTaxYearInput={handleTaxYear}
                                            />;
                                    case 'CA':
                                        return <CAForm
                                            handleSalaryInput={handleSetSalary}
                                            salary={salary}
                                            handleTaxYearInput={handleTaxYear}
                                            />;
                                    case 'NG':
                                        return <NGForm
                                            handleSalaryInput={handleSetSalary}
                                            salary={salary}
                                            handleTaxYearInput={handleTaxYear}
                                            />;
                                    default:
                                        return <GBForm 
                                            handleSalaryInput={handleSetSalary}
                                            salary={salary}
                                            handleTaxYearInput={handleTaxYear}
                                            />;
                                }
                            })()}
                    </>
                ) : (
                    <>
                        {children}
                    </>
                )}
            </main>
            <footer className={footerStyles.footerContainer}>
            {!home && (
                    <div>
                        <Link href="/">
                            <a>Back to home</a>
                        </Link>
                    </div>
                )}
                <span>Disclaimer: This is only an approximation of your net pay. There are many other variables, for an exact estimate contact your local tax authority. Please do not under any circumstance let this constitute as financial advice</span>
            </footer>
        </>
    )
}