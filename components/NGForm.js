import mainStyles from '../styles/main.module.css'

export default function NGForm (props) {
    const GrossSalary = props.salary * 1

    // Salary variables
    const yearlyGross = GrossSalary
    const monthlyGross = (GrossSalary)/12
    const weeklyGross = (GrossSalary)/52
    const dailyGross = (GrossSalary)/365

    // Tax paid variables

    const yearlyTax = (x) => {
        let tax
        if (x > 3200000) {
            tax = (x - 3200000) * .24 + (1600000 * .21) + (500000 * .19) + (500000 * .15) + (300000 * .11) + (300000 * .07)
            return tax
        } else if (x > 1600000) {
            tax = (x - 1600000) * .21 + (500000 * .19) + (500000 * .15) + (300000 * .11) + (300000 * .07)
            return tax
        } else if (x > 1100000) {
            tax = (x - 1100000) * .19 + (500000 * .15) + (300000 * .11) + (300000 * .07)
            return tax
        } else if (x > 600000) {
            tax = (x - 600000) * .15 + (300000 * .11) + (300000 * .07)
            return tax
        } else if (x > 300000) {
            tax = (x - 300000) * .11 + (300000 * .07)
            return tax
        } else {
            tax = x * .07
            return tax
        }
    }
    const monthlyTax = yearlyTax(GrossSalary)/12
    const weeklyTax = yearlyTax(GrossSalary)/52
    const dailyTax = yearlyTax(GrossSalary)/365

    //National Housing Fund variables
    const yearlyNHF = (x) => {
        let NHFC
        if (x > 3000) {
            NHFC = x * .025
            return NHFC
        } else {
            NHFC = 0
            return NHFC
        }
    }
    const monthlyNHF = yearlyNHF(GrossSalary)/12
    const weeklyNHF = yearlyNHF(GrossSalary)/52
    const dailyNHF = yearlyNHF(GrossSalary)/365

    // Take Home variables

    const yearlyTakeHome = GrossSalary - yearlyTax(GrossSalary) - yearlyNHF(GrossSalary)
    const monthlyTakeHome = yearlyTakeHome/12
    const weeklyTakeHome = yearlyTakeHome/52
    const dailyTakeHome = yearlyTakeHome/365

    return (
        <>
        <div className={mainStyles.formContentWrapper}>
            <div className={mainStyles.formContent}>
                <form className={mainStyles.form} onSubmit={props.handleSubmit}>
                    <div className={mainStyles.formFirstRow}>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Gross Salary</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <input onChange={props.handleSalaryInput} className={mainStyles.inputText} name="salary" type="text" placeholder="0" value={props.salary}/>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warning}>Please enter an email</p>
                            )} */}
                        </div>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Tax Year</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <select className={mainStyles.inputSelect} onChange={props.handleTaxYearInput}>
                                        <option value="2021-22">2021-22</option>
                                    </select>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warning}>Please enter an email</p>
                            )} */}
                        </div>
                    </div>
                </form>
                <div className={mainStyles.resultsWrapper}>
                    <table className={mainStyles.table}>
                        <thead>
                            <tr>
                                <th className={mainStyles.th}></th>
                                <th className={mainStyles.th}>Yearly</th>
                                <th className={mainStyles.th}>Monthly</th>
                                <th className={mainStyles.th}>Weekly</th>
                                <th className={mainStyles.th}>Daily</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th className={mainStyles.th}>Gross Salary</th>
                                <td className={mainStyles.td}>{yearlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyGross.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Tax</th>
                                <td className={mainStyles.td}>{yearlyTax(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTax.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>National Housing Fund</th>
                                <td className={mainStyles.td}>{yearlyNHF(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyNHF.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyNHF.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyNHF.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Net Pay</th>
                                <td className={mainStyles.td}>{yearlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTakeHome.toFixed(2)}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </>
    )
}