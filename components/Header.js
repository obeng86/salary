import headerStyles from '../styles/header.module.css'
import Link from 'next/link'
import Image from 'next/image'
import Countries from './Countries'

export default function Header (props) {

    const flagThumb = props.countryThumbnail

    const toggleMobileMenu = () => {
        const mobileMenu = document.querySelector('[class^="header_mainMenu_"]')
        const countryMenu = document.querySelector('[class^="header_flags_"]')
        
        countryMenu.removeAttribute('id')
        
        if (mobileMenu.id == 'active' ) {
            mobileMenu.removeAttribute('id')
          } else {
            mobileMenu.id = 'active'
          }
    }

    const toggleCountryMenu = () => {
        const countryMenu = document.querySelector('[class^="header_flags_"]')
        const mobileMenu = document.querySelector('[class^="header_mainMenu_"]')

        mobileMenu.removeAttribute('id')
        
        if (countryMenu.id == 'active' ) {
            countryMenu.removeAttribute('id')
          } else {
            countryMenu.id = 'active'
          }
    }

    const collapseMenuOnHeaderLinkClick = () => {
        const mobileMenu = document.querySelector('[class^="header_mainMenu_"]')
        const countryMenu = document.querySelector('[class^="header_flags_"]')
        
        mobileMenu.removeAttribute('id')
        countryMenu.removeAttribute('id')
    }
    const collapseFlagMenuOnLinkClick = () => {
        const mobileMenu = document.querySelector('[class^="header_mainMenu_"]')
        const countryMenu = document.querySelector('[class^="header_flags_"]')
        
        mobileMenu.removeAttribute('id')
        countryMenu.removeAttribute('id')
    }

    return (
        <header className={headerStyles.headerContainer}>
            <nav className={headerStyles.mainMenu}>
                <div  className={headerStyles.menuLinkWrapper} onClick={collapseMenuOnHeaderLinkClick} >
                    <Link href="/">
                        <a className={headerStyles.menuLink}>Home</a>
                    </Link>
                </div>
                <div className={headerStyles.menuLinkWrapper} onClick={collapseMenuOnHeaderLinkClick} >
                    <Link href="/about" >
                        <a className={headerStyles.menuLink}>About</a>
                    </Link>
                </div>
                <div className={headerStyles.menuLinkWrapper} onClick={collapseMenuOnHeaderLinkClick} >
                    <Link href="/contact">
                        <a className={headerStyles.menuLink}>Contact</a>
                    </Link>
                </div>
            </nav>
            <a onClick={toggleMobileMenu} id="mobileMenuBtn" className={headerStyles.mobileMenuBtn} href="#">
                <span className={headerStyles.mobileMenuSpan}></span>
            </a>
            <div className={headerStyles.logo}>
                <div className={headerStyles.desktopLogo} >
                    <Image src="/salarycrunchlogowhite.png" alt="Salary crunch logo" width={158} height={37} />
                </div>
                <div className={headerStyles.mobileLogo} >
                <Image src="/salarycrunchlogomobilewhite.png" alt="Salary crunch logo" width={37} height={37} />
                </div>
            </div>
            
            {/* <div className={headerStyles.socials}>
                <div className={headerStyles.socialIcon}>
                    <FontAwesomeIcon icon={faLinkedinIn} size="xs" className={headerStyles.svg} />
                </div>
                <div className={headerStyles.socialIcon}>
                    <FontAwesomeIcon icon={faTwitter} size="xs" className={headerStyles.svg} />
                </div>
            </div> */}
            <div className={headerStyles.flagSelectorWrapper} >
                <div className={headerStyles.flagSelector}>
                {flagThumb.length > 0 &&        
                    <button onClick={toggleCountryMenu} className={headerStyles.flagSelectBtn} value={props.countryID}>
                        <Image src={flagThumb} alt={`${props.countryName} + ' flag thumbnail'`} width={21} height={14} />
                    </button>
                 }
                    
                </div>
            </div>
            <div className={headerStyles.flags}>
                <ul className={headerStyles.flagsUl}>
                    {
                        Countries.map((item, i) => {
                            return(
                                <li onClick={props.handleCountryChange} key={i} className={headerStyles.flagsLi}>
                                    <Image src={item.flagThumb} alt={`${item.name} + ' flag thumbnail'`} width={31} height={21} />
                                    <span className={headerStyles.flagName}>{item.name}</span>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        </header>
    )
}