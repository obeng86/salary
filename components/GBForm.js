import mainStyles from '../styles/main.module.css'

export default function GBForm (props) {
    const GrossSalary = props.salary * 1

    // Salary variables
    const yearlyGross = GrossSalary
    const monthlyGross = (GrossSalary)/12
    const weeklyGross = (GrossSalary)/52
    const dailyGross = (GrossSalary)/365

    // Tax paid variables

    const yearlyTax = (x) => {
        let tax
        if (x > 150000) {
            tax = (x - 150000) * .45 + (150000 - 50271) * .4 + (50270 - 12571) * .2
            return tax
        } else if (x > 50270) {
            tax = (x - 50270) * .4 + (50270 - 12571) * .2
            return tax
        } else if (x > 12570) {
            tax = (x - 12570) * .2
            return tax
        } else {
            tax = 0
            return tax
        }
    }
    const monthlyTax = yearlyTax(GrossSalary)/12
    const weeklyTax = yearlyTax(GrossSalary)/52
    const dailyTax = yearlyTax(GrossSalary)/365

    //National Insurance variables
    const yearlyNI = (x) => {
        let NIC
        if (x > 50276) {
            NIC = (x - 50276) * .02 + (50276 - 9566) * .12
            return NIC
        } else if (x > 9566) {
            NIC = (x - 9566) * .12
            return NIC
        } else {
            NIC = 0
            return NIC
        }
    }
    const monthlyNI = yearlyNI(GrossSalary)/12
    const weeklyNI = yearlyNI(GrossSalary)/52
    const dailyNI = yearlyNI(GrossSalary)/365

    // Take Home variables

    const yearlyTakeHome = GrossSalary - yearlyTax(GrossSalary) - yearlyNI(GrossSalary)
    const monthlyTakeHome = yearlyTakeHome/12
    const weeklyTakeHome = yearlyTakeHome/52
    const dailyTakeHome = yearlyTakeHome/365

    return (
        <>
        <div className={mainStyles.formContentWrapper}>
            <div className={mainStyles.formContent}>
                <form className={mainStyles.form} onSubmit={props.handleSubmit}>
                    <div className={mainStyles.formFirstRow}>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Gross Salary</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <input onChange={props.handleSalaryInput} className={mainStyles.inputText} name="salary" type="text" placeholder="0" value={props.salary}/>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warning}>Please enter an email</p>
                            )} */}
                        </div>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Tax Year</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <select className={mainStyles.inputSelect} onChange={props.handleTaxYearInput}>
                                        <option value="2021-22">2021-22</option>
                                    </select>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warning}>Please enter an email</p>
                            )} */}
                        </div>
                    </div>
                </form>
                <div className={mainStyles.resultsWrapper}>
                    <table className={mainStyles.table}>
                        <thead>
                            <tr>
                                <th className={mainStyles.th}></th>
                                <th className={mainStyles.th}>Yearly</th>
                                <th className={mainStyles.th}>Monthly</th>
                                <th className={mainStyles.th}>Weekly</th>
                                <th className={mainStyles.th}>Daily</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th className={mainStyles.th}>Gross Salary</th>
                                <td className={mainStyles.td}>{yearlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyGross.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Tax</th>
                                <td className={mainStyles.td}>{yearlyTax(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTax.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>National Insurance</th>
                                <td className={mainStyles.td}>{yearlyNI(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyNI.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyNI.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyNI.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Take Home</th>
                                <td className={mainStyles.td}>{yearlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTakeHome.toFixed(2)}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </>
    )
}