const Countries = [
    {
        id: "AU",
        name: "Australia",
        flag: "https://upload.wikimedia.org/wikipedia/commons/8/88/Flag_of_Australia_%28converted%29.svg",
        flagThumb: "/authumb.png"
    },
    {
        id: "CA",
        name: "Canada",
        flag: "https://upload.wikimedia.org/wikipedia/commons/d/d9/Flag_of_Canada_%28Pantone%29.svg",
        flagThumb: "/cathumb.png"
    },
    {
        id: "GB",
        name: "United Kingdom",
        flag: "https://upload.wikimedia.org/wikipedia/commons/6/6e/United_Kingdom.svg",
        flagThumb: "/gbthumb.png"
    },
    {
        id: "GH",
        name: "Ghana",
        flag: "https://upload.wikimedia.org/wikipedia/commons/1/19/Flag_of_Ghana.svg",
        flagThumb: "/ghthumb.png"
    },
    {
        id: "NG",
        name: "Nigeria",
        flag: "https://upload.wikimedia.org/wikipedia/commons/7/79/Flag_of_Nigeria.svg",
        flagThumb: "/ngthumb.png"
    },
    {
        id: "US",
        name: "United States",
        flag: "https://upload.wikimedia.org/wikipedia/commons/a/a4/Flag_of_the_United_States.svg",
        flagThumb: "/usthumb.png"
    }
]

export default Countries