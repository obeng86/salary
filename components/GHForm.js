import mainStyles from '../styles/main.module.css'

export default function GHForm (props) {
    const GrossSalary = props.salary * 1

    // Salary variables
    const yearlyGross = GrossSalary
    const monthlyGross = (GrossSalary)/12
    const weeklyGross = (GrossSalary)/52
    const dailyGross = (GrossSalary)/365

    // Tax paid variables

    const yearlyTax = (x) => {
        let tax
        if (x > 240000) {
            tax = (x - 240000) * .3 + (197532 * .25) + (36000 * .175) + (1440 * .1) + (1200 * .05)
            return tax
        } else if (x > 42468) {
            tax = (x - 42468) * .25 + (36000 * .175) + (1440 * .1) + (1200 * .05)
            return tax
        } else if (x > 6468) {
            tax = (x - 6468) * .175 + (1440 * .1) + (1200 * .05)
            return tax
        } else if (x > 5028) {
            tax = (x - 5028) * .1 + (1200 * .05)
            return tax
        } else if (x > 3828) {
            tax = (x - 3828) * .05
            return tax
        } else {
            tax = 0
            return tax
        }
    }
    const monthlyTax = yearlyTax(GrossSalary)/12
    const weeklyTax = yearlyTax(GrossSalary)/52
    const dailyTax = yearlyTax(GrossSalary)/365

    //SSNIT variables
    const yearlySSNIT = (x) => {
        let SSNITC
        if (x > 2962.38) {
            SSNITC = (x/100)*18.5
            return SSNITC
        } else {
            SSNITC = 0
            return SSNITC
        }
    }
    const monthlySSNIT = yearlySSNIT(GrossSalary)/12
    const weeklySSNIT = yearlySSNIT(GrossSalary)/52
    const dailySSNIT = yearlySSNIT(GrossSalary)/365

    // Take Home variables

    const yearlyTakeHome = GrossSalary - yearlyTax(GrossSalary) - yearlySSNIT(GrossSalary)
    const monthlyTakeHome = yearlyTakeHome/12
    const weeklyTakeHome = yearlyTakeHome/52
    const dailyTakeHome = yearlyTakeHome/365

        return (
            <>
        <div className={mainStyles.formContentWrapper}>
            <div className={mainStyles.formContent}>
                <form className={mainStyles.form} onSubmit={props.handleSubmit}>
                    <div className={mainStyles.formFirstRow}>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Gross Salary</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <input onChange={props.handleSalaryInput} className={mainStyles.inputText} name="salary" type="text" placeholder="0" value={props.salary}/>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warSSNITng}>Please enter an email</p>
                            )} */}
                        </div>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Tax Year</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <select className={mainStyles.inputSelect} onChange={props.handleTaxYearInput}>
                                        <option value="2021-22">2021-22</option>
                                    </select>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warSSNITng}>Please enter an email</p>
                            )} */}
                        </div>
                    </div>
                </form>
                <div className={mainStyles.resultsWrapper}>
                    <table className={mainStyles.table}>
                        <thead>
                            <tr>
                                <th className={mainStyles.th}></th>
                                <th className={mainStyles.th}>Yearly</th>
                                <th className={mainStyles.th}>Monthly</th>
                                <th className={mainStyles.th}>Weekly</th>
                                <th className={mainStyles.th}>Daily</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th className={mainStyles.th}>Gross Salary</th>
                                <td className={mainStyles.td}>{yearlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyGross.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Tax</th>
                                <td className={mainStyles.td}>{yearlyTax(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTax.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>SSNIT</th>
                                <td className={mainStyles.td}>{yearlySSNIT(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlySSNIT.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklySSNIT.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailySSNIT.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Take Home</th>
                                <td className={mainStyles.td}>{yearlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTakeHome.toFixed(2)}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </>
        )
}