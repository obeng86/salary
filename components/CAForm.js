import mainStyles from '../styles/main.module.css'

export default function CAForm (props) {
    const GrossSalary = props.salary * 1

    // Salary variables
    const yearlyGross = GrossSalary
    const monthlyGross = (GrossSalary)/12
    const weeklyGross = (GrossSalary)/52
    const dailyGross = (GrossSalary)/365

    // Tax paid variables

    const yearlyTax = (x) => {
        let tax
        if (x > 216511) {
            tax = (x - 216511) * .33 + 50141
            return tax
        } else if (x > 151978) {
            tax = (x - 151978) * .29 + 31426
            return tax
        } else if (x > 98040) {
            tax = (x - 98040) * .26 + 17402
            return tax
        } else if (x > 49020) {
            tax = (x - 49020) * .205 + 7353
            return tax
        } else {
            tax = x * .15
            return tax
        }
    }
    const monthlyTax = yearlyTax(GrossSalary)/12
    const weeklyTax = yearlyTax(GrossSalary)/52
    const dailyTax = yearlyTax(GrossSalary)/365

    //Pension Plan variables
    const yearlyPP = (x) => {
        let PPC
        if (x > 61600) {
            PPC = (61600 - 3500) * .0545
            return PPC
        } else if (x > 3500) {
            PPC = (x - 3500) * .0545
            return PPC
        } else {
            PPC = 0
            return PPC
        }
    }
    const monthlyPP = yearlyPP(GrossSalary)/12
    const weeklyPP = yearlyPP(GrossSalary)/52
    const dailyPP = yearlyPP(GrossSalary)/365

    //Employment Insurance variables
    const yearlyEI = (x) => {
        let EIC
        if (x > 56300) {
            EIC = 56300 * .0158
            return EIC
        }  else {
            EIC = x * 0.158
            return EIC
        }
    }
    const monthlyEI = yearlyEI(GrossSalary)/12
    const weeklyEI = yearlyEI(GrossSalary)/52
    const dailyEI = yearlyEI(GrossSalary)/365

    // Take Home variables

    const yearlyTakeHome = GrossSalary - yearlyTax(GrossSalary) - yearlyEI(GrossSalary) - yearlyPP(GrossSalary)
    const monthlyTakeHome = yearlyTakeHome/12
    const weeklyTakeHome = yearlyTakeHome/52
    const dailyTakeHome = yearlyTakeHome/365

    return (
        <>
        <div className={mainStyles.formContentWrapper}>
            <div className={mainStyles.formContent}>
                <form className={mainStyles.form} onSubmit={props.handleSubmit}>
                    <div className={mainStyles.formFirstRow}>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Gross Salary</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <input onChange={props.handleSalaryInput} className={mainStyles.inputText} name="salary" type="text" placeholder="0" value={props.salary}/>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warning}>Please enter an email</p>
                            )} */}
                        </div>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Tax Year</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <select className={mainStyles.inputSelect} onChange={props.handleTaxYearInput}>
                                        <option value="2021-22">2021-22</option>
                                    </select>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warning}>Please enter an email</p>
                            )} */}
                        </div>
                    </div>
                </form>
                <div className={mainStyles.resultsWrapper}>
                    <table className={mainStyles.table}>
                        <thead>
                            <tr>
                                <th className={mainStyles.th}></th>
                                <th className={mainStyles.th}>Yearly</th>
                                <th className={mainStyles.th}>Monthly</th>
                                <th className={mainStyles.th}>Weekly</th>
                                <th className={mainStyles.th}>Daily</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th className={mainStyles.th}>Gross Salary</th>
                                <td className={mainStyles.td}>{yearlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyGross.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Federal Tax</th>
                                <td className={mainStyles.td}>{yearlyTax(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTax.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Pension Plan</th>
                                <td className={mainStyles.td}>{yearlyPP(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyPP.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyPP.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyPP.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Employment Insurance</th>
                                <td className={mainStyles.td}>{yearlyEI(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyEI.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyEI.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyEI.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Take Home</th>
                                <td className={mainStyles.td}>{yearlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTakeHome.toFixed(2)}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </>
    )
}