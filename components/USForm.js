import mainStyles from '../styles/main.module.css'
import Countries from './Countries'

export default function USForm (props) {
    const GrossSalary = props.salary * 1

    // Salary variables
    const yearlyGross = GrossSalary
    const monthlyGross = (GrossSalary)/12
    const weeklyGross = (GrossSalary)/52
    const dailyGross = (GrossSalary)/365

    // Federal Tax paid variables

    const yearlyTax = (x) => {
        let tax
        if (x > 523600) {
            tax = (x - 523600) * .37 + (523600 - 209425) * .35 + (209425 - 164925) * .32 + (164925 - 86375) * .24 + (86375 - 40525) * .22 + (40525 - 9950) * .12 + (9950 * .1)
            return tax
        } else if (x > 209425) {
            tax = (x - 209425) * .35 + (209425 - 164925) * .32 + (164925 - 86375) * .24 + (86375 - 40525) * .22 + (40525 - 9950) * .12 + (9950 * .1)
            return tax
        } else if (x > 164925) {
            tax = (x - 164925) * .32 + (164925 - 86375) * .24 + (86375 - 40525) * .22 + (40525 - 9950) * .12 + (9950 * .1)
            return tax
        } else if (x > 86375) {
            tax = (x - 86375) * .24 + (86375 - 40525) * .22 + (40525 - 9950) * .12 + (9950 * .1)
            return tax
        } else if (x > 40525) {
            tax = (x - 40525) * .22 + (40525 - 9950) * .12 + (9950 * .1)
            return tax
        } else if (x > 9950) {
            tax = (x - 9950) * .12 + (9950 * .1)
            return tax
        } else {
            tax = x * .1
            return tax
        }
    }
    const monthlyTax = yearlyTax(GrossSalary)/12
    const weeklyTax = yearlyTax(GrossSalary)/52
    const dailyTax = yearlyTax(GrossSalary)/365

    //FICA variables

    //Social Security
    const yearlySS = (x) => {
        let SS
        if (x > 142800) {
            SS = 8853.60
            return SS
        } else {
            SS = x * .062
            return SS
        }
    }
    const monthlySS = yearlySS(GrossSalary)/12
    const weeklySS = yearlySS(GrossSalary)/52
    const dailySS = yearlySS(GrossSalary)/365

    // Medicare
    const yearlyMediCare = (x) => {
        let mediCare
        if (x > 200000) {
            mediCare = (x - 200000) * .0235 + (200000 * .0145)
            return mediCare
        }  else {
            mediCare = x * .0145
            return mediCare
        }
    }
    const monthlyMediCare = yearlyMediCare(GrossSalary)/12
    const weeklyMediCare = yearlyMediCare(GrossSalary)/52
    const dailyMediCare = yearlyMediCare(GrossSalary)/365

    // Take Home variables

    const yearlyTakeHome = GrossSalary - yearlyTax(GrossSalary) - yearlySS(GrossSalary) - yearlyMediCare(GrossSalary)
    const monthlyTakeHome = yearlyTakeHome/12
    const weeklyTakeHome = yearlyTakeHome/52
    const dailyTakeHome = yearlyTakeHome/365

    

    return (
        <>
        <div className={mainStyles.formContentWrapper}>
            <div className={mainStyles.formContent}>
                <form className={mainStyles.form} onSubmit={props.handleSubmit}>
                    <div className={mainStyles.formFirstRow}>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Gross Salary</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <input onChange={props.handleSalaryInput} className={mainStyles.inputText} name="salary" type="text" placeholder="0" value={props.salary}/>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warning}>Please enter an email</p>
                            )} */}
                        </div>
                        <div className={mainStyles.firstRowFormGroup}>
                            <div className={mainStyles.label}>Tax Year</div>
                            <p className={mainStyles.para}>
                                <span className={mainStyles.span}>
                                    <select className={mainStyles.inputSelect} onChange={props.handleTaxYearInput}>
                                        <option value="2021-22">2021-22</option>
                                    </select>
                                </span>
                            </p>
                            {/* {errors?.email && (
                            <p className={mainStyles.warning}>Please enter an email</p>
                            )} */}
                        </div>
                    </div>
                </form>
                <div className={mainStyles.resultsWrapper}>
                    <table className={mainStyles.table}>
                        <thead>
                            <tr>
                                <th className={mainStyles.th}></th>
                                <th className={mainStyles.th}>Yearly</th>
                                <th className={mainStyles.th}>Monthly</th>
                                <th className={mainStyles.th}>Weekly</th>
                                <th className={mainStyles.th}>Daily</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th className={mainStyles.th}>Gross Salary</th>
                                <td className={mainStyles.td}>{yearlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyGross.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyGross.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Federal Income Tax</th>
                                <td className={mainStyles.td}>{yearlyTax(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTax.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTax.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Social Security Tax</th>
                                <td className={mainStyles.td}>{yearlySS(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlySS.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklySS.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailySS.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Medicare</th>
                                <td className={mainStyles.td}>{yearlyMediCare(yearlyGross).toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyMediCare.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyMediCare.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyMediCare.toFixed(2)}</td>
                            </tr>
                            <tr>
                                <th className={mainStyles.th}>Net Pay</th>
                                <td className={mainStyles.td}>{yearlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{monthlyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{weeklyTakeHome.toFixed(2)}</td>
                                <td className={mainStyles.td}>{dailyTakeHome.toFixed(2)}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </>
    )
}