export async function fetchCountryData() {
    const ipApi = process.env.IPSTACK_API
    const countryObjReq = await fetch(`http://api.ipstack.com/check?access_key=${ipApi}`)
    const countryObj = await countryObjReq.json()
    return countryObj
}